$.ajaxSetup({
    async: false,
});

var urlParams = new URLSearchParams(window.location.search);
var lang = urlParams.get("lang");
if (!lang) {
    window.location.replace("/courses.html");
}

var data;
$.getJSON("/courses/" + lang + ".json", function (json) {
    data = json;
});
if (!data) {
    window.location.replace("/courses.html");
}

$("#title").text(data["name"]);
$("#desc").text(data["desc"]);
console.log(data["name"]);

$.each(data["courses"], (courseIndex, courseData) => {
    var courseDiv = $("<div />", {
        class: "course",
    }).appendTo("main");
    $("<div />", {
        text: courseData["name"],
        class: "label",
    }).appendTo(courseDiv);
    $.each(courseData["lessons"], (lessonIndex, lessonData) => {
        var lessonLink = $("<a />", {
            class: "lesson",
            href:
                "/les/son.html?lang=" +
                lang +
                "&course=" +
                courseIndex +
                "&lesson=" +
                lessonIndex,
        }).appendTo(courseDiv);
        $("<div />", {
            text: lessonData["name"],
            class: "lessonTitle",
        }).appendTo(lessonLink);
        $("<div />", {
            text: lessonData["desc"],
            class: "lessonDesc",
        }).appendTo(lessonLink);
    });
});
