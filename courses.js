$.ajaxSetup({
    async: false,
});

var courseList = $("#courseList");
var languages = ["nhaque", "tokipona", "ijeda", "prograza", "test"];
$.each(languages, (index, language) => {
    $.getJSON("/courses/" + language + ".json", function (json) {
        var button = $("<a />", {
            class: "language button",
            href: "/index.html?lang=" + language,
        }).appendTo(courseList);

        var imageContainer = $("<div />", {
            class: "img",
        }).appendTo(button);

        $("<img />", {
            src: json["flag"],
        }).appendTo(imageContainer);

        $("<div />", {
            class: "name",
            text: json["name"],
        }).appendTo(button);

        $("<div />", {
            class: "creator",
            text: "by " + json["creator"],
        }).appendTo(button);
    });
});
