$.ajaxSetup({
    async: false,
});

var ans;
var progress = 0;
var data;

var urlParams = new URLSearchParams(window.location.search);
var lang = urlParams.get("lang");
var course = urlParams.get("course");
var lesson = urlParams.get("lesson");
var selected = null;
var selectedID = null;
if (!lang) {
    window.location.replace("/courses.html");
}

$("#back-button").attr("href", "/index.html?lang=" + lang);

$.getJSON("/courses/" + lang + ".json", function (json) {
    data = json;
});

const container = $("#container");

function makeNew() {
    if (
        progress >=
        data["courses"][course]["lessons"][lesson]["questions"].length
    ) {
        window.location.replace("/index.html?lang=" + lang);
    }
    question =
        data["courses"][course]["lessons"][lesson]["questions"][progress];
    if (question["type"] == "write") {
        direction = Math.random() < (question["direction"] + 1) / 2;
        ans = question["content"][+!direction];
        container.empty();
        $("<div />", {
            id: "title",
            text: "Write this in " + data[direction ? "name" : "transl"],
        }).appendTo(container);
        var prompt = $("<div />", {
            id: "prompt",
        }).appendTo(container);
        $("<img />", {
            src: "/assets/alien.png",
        }).appendTo(prompt);
        $("<div />", {
            id: "translation",
            class: "border",
            text: question["content"][+direction],
        }).appendTo(prompt);
        $("<textarea />", {
            id: "textarea",
            placeholder: "Type here in " + data[direction ? "name" : "transl"],
        })
            .on("keyup", updateResult)
            .appendTo(container);
        $("<button />", {
            id: "check-button",
            text: "CHECK",
            disabled: true,
        })
            .click(() => {
                check($("#textarea").val());
            })
            .appendTo(container);
    } else if (question["type"] == "choice") {
        ans = question["correct"];
        container.empty();
        $("<div />", {
            id: "title",
            text: "Choose the correct translation",
        }).appendTo(container);
        var prompt = $("<div />", {
            id: "prompt",
        }).appendTo(container);
        $("<img />", {
            src: "/assets/alien.png",
        }).appendTo(prompt);
        $("<div />", {
            id: "translation",
            class: "border",
            text: question["prompt"],
        }).appendTo(prompt);
        var choices = [];
        $.each(question["choices"], (index, choice) => {
            choices.splice(
                Math.random() * choices.length,
                0,
                $("<button />", {
                    id: "choice" + index,
                    class: "button choice",
                    text: choice,
                }).click(() => check(index))
            );
        });
        $.each(choices, (_, choice) => {
            choice.appendTo(container);
        });
    } else if (question["type"] == "match") {
        container.empty();
        $("<div />", {
            id: "title",
            text: "Match the options",
        }).appendTo(container);

        var table = $("<div />", { id: "choices" }).appendTo(container);
        var choices = [];
        $.each(Object.keys(question["pairs"]), (index, key) => {
            console.log(index, key);
            choices.splice(
                Math.random() * choices.length,
                0,
                $("<button />", {
                    id: "key" + index,
                    value: index,
                    class: "button choice",
                    text: key,
                })
            );
        });

        $.each(Object.values(question["pairs"]), (index, pair) => {
            choices.splice(
                Math.random() * choices.length,
                0,
                $("<button />", {
                    id: "pair" + index,
                    value: index,
                    class: "button choice",
                    text: pair,
                })
            );
        });

        $.each(choices, (_, choice) => {
            choice
                .on("click", function () {
                    console.log(this.value, this.id);
                    if (!selected) {
                        selected = this.value;
                        selectedID = this.id;
                        $(this).addClass("selected");
                    } else {
                        if (this.value == selected) {
                            $(this).attr("disabled", true);
                            $("#" + selectedID).attr("disabled", true);
                        } else {
                            alert("wrong");
                        }
                        selected = null;
                        selectedID = null;
                        $(".button").removeClass("selected");

                        var disabled = true;
                        $.each($(".choice"), (_, button) => {
                            if (!$(button).is(":disabled")) {
                                disabled = false;
                            }
                        });

                        if (disabled) {
                            progress++;
                            alert("done");
                            makeNew();
                        }
                    }
                })
                .appendTo(table);
        });
    } else if (question["type"] == "image") {
        ans = question["correct"];
        container.empty();

        var prompt = $("<div />", {
            id: "title",
            text: 'Which one of these is "' + question["prompt"] + '"?',
        }).appendTo(container);

        var imgContainer = $("<div />", { class: "img-container" }).appendTo(
            container
        );

        var buttons = [];

        $.each(Object.keys(question["choices"]), (index, choice) => {
            var button = $("<button />", {
                id: "choice" + index,
                class: "button image-container",
            }).click(() => check(index));

            $("<img />", {
                class: "choice-image",
                src: "/assets/images/" + question["choices"][choice],
            }).appendTo(button);
            $("<div />", {
                text: choice,
            }).appendTo(button);

            buttons.splice(Math.random() * buttons.length, 0, button);
        });

        $.each(buttons, function () {
            $(this).appendTo(imgContainer);
        });
    } else {
        console.log("Not a valid question type!");
        console.log(question["type"]);
    }
}

function updateResult() {
    if (!$("#textarea").val()) {
        $("#check-button").prop("disabled", true);
    } else {
        $("#check-button").prop("disabled", false);
    }
}

function check(val) {
    if (val == ans) {
        progress++;
        alert("correct");
        makeNew();
    } else {
        alert("wrong: " + ans);
    }
}

makeNew();
